from Entorno.Simbolo import Simbolo


class Entorno:
    def __init__(self, anterior=None):
        self.anterior = anterior
        self.tablaSimbolos = {}

    def nuevoSimbolo(self, simbolo: Simbolo):
        s = self.tablaSimbolos.get(simbolo.nombre)

        if s == None:
            self.tablaSimbolos[simbolo.nombre] = simbolo
            return "ok"

        return "Ya existe un símbolo con esa llave"

    def editarSimbolo(self, llave, nuevo: Simbolo):
        s = self.tablaSimbolos.get(llave)
        envr = self
        while envr is not None:
            s = envr.tablaSimbolos.get(llave.lower())
            if s is not None:
                envr.tablaSimbolos[llave] = nuevo
                return "Se ha editado el simbolo " + llave
            envr = envr.anterior
        return "No existe símbolo con llave: " + llave

    def eliminarSimbolo(self, llave):
        s = self.tablaSimbolos.get(llave)

        if s != None:
            del self.tablaSimbolos[llave]
            return "ok"

        return "El símbolo a eliminar no existe"

    def buscarSimbolo(self, llave):
        ent = self

        while ent != None:
            s = ent.tablaSimbolos.get(llave)
            if s != None:
                return s
            ent = ent.anterior

        return None
