import os

import Entorno.Simbolo
from Interface.Instruccion import Instruccion
from Expresiones.Identificador import Identificador
import sys


class TablaSimbolos(Instruccion):
    def __init__(self, nombre):
        self.nombre = nombre

    def ejecutar(self, entorno):
        actual = entorno
        nombreArchivo = ""
        if isinstance(self.nombre,Identificador):
            nombreArchivo = self.nombre.identificador
        else:
            nombreArchivo = self.nombre.valor
        contador=1
        f = open(nombreArchivo + '.html', 'w', encoding='utf-8')
        f.write("<html><body>\n")
        f.write('<TABLE BORDER="1" cellborder = "1" cellspacing="10" cellpadding="10">\n')
        f.write('<style type="text/css"> tr{text-align: center;} table {border-collapse: collapse; background: whitesmoke; margin: auto;} body{background: #DC7633; font-family: sans-serif;}</style>\n')
        f.write('<tr><td colspan="4"><b>Tabla de Simbolos Entorno</b></td></tr>\n')
        f.write('<tr>\n')
        f.write('<td>' + "Tipo" + '</td>\n')
        f.write('<td>' + "Nombre" + '</td>\n')
        f.write('<td>' + "Valor" + '</td>\n')
        f.write('<td>' + "Linea" + '</td>\n')
        f.write('</tr>\n')

        for i in actual.tablaSimbolos:
            valor = actual.tablaSimbolos[i]
            f.write('<tr>\n')
            if valor.tipo == 'FUNC':
                f.write('<td>' + str(valor.valor.tipo) + '</td>\n')
                f.write('<td>' + str(valor.nombre) + '</td>\n')
                f.write('<td>' + "FUNC" + '</td>\n')
                f.write('<td>' + str(valor.linea) + '</td>\n')
            else:
                f.write('<td>' + str(valor.tipo) + '</td>\n')
                f.write('<td>' + str(valor.nombre) + '</td>\n')
                if isinstance(valor.valor, str) or isinstance(valor.valor, int) or isinstance(valor.valor,
                                                                                              float) or isinstance(
                        valor.valor, bool) or isinstance(valor.valor, list):
                    f.write('<td>' + str(valor.valor) + '</td>\n')
                else:
                    f.write('<td>' + " " + '</td>\n')
                f.write('<td>' + str(valor.linea) + '</td>\n')
            f.write('</tr>\n')
            contador += 1

        f.write('</TABLE>\n')
        f.write("</body></html>")
        f.close()
        os.system(nombreArchivo+'.html')
