from Interface.Instruccion import Instruccion


class Aritmetica(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = None

    def ejecutar(self, entorno):
        izq = self.hijoIzq.ejecutar(entorno)
        der = self.hijoDer.ejecutar(entorno)

        if self.operador == '+':
            if self.hijoIzq.tipo == 'INT' or isinstance(izq,int) and (izq is not True and izq is not False):
                if self.hijoDer.tipo== 'INT' or isinstance(der,int):
                    self.tipo = 'INT'
                    return self.obtenerValor('INT', izq) + self.obtenerValor('INT', der)
                elif self.hijoDer.tipo == 'STRING' or isinstance(der,str):
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('INT', izq)) + str(self.obtenerValor('STRING', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo='DOUBLE'
                    return float(self.obtenerValor('INT', izq)) + float(self.obtenerValor('DOUBLE', der))
                elif self.hijoDer.tipo == 'BOOLEAN' or isinstance(der,bool):
                    self.tipo="INT"
                    return self.obtenerValor('INT',izq) + int((1 if der else 0))

            elif self.hijoIzq.tipo == 'STRING' or isinstance(izq,str):
                if self.hijoDer.tipo == 'STRING' or isinstance(der,str):
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('STRING', der))
                elif (self.hijoDer.tipo == 'INT' or isinstance(der,int)) and (der is not True and der is not False):
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('DOUBLE', der))
                elif self.hijoDer.tipo == 'BOOLEAN' or isinstance(der,bool):
                    self.tipo="STRING"
                    return str(self.obtenerValor('STRING',izq)) + str(("true" if der else "false"))

            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(izq,float):
                if self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) + float(self.obtenerValor('DOUBLE', der))
                elif self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) + float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'STRING' or isinstance(der,str):
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('DOUBLE', izq)) + str(self.obtenerValor('STRING', der))
                elif self.hijoDer.tipo == 'BOOLEAN' or isinstance(der,bool):
                    self.tipo="DOUBLE"
                    return self.obtenerValor('DOUBLE',izq) + float((1 if der else 0))

            elif self.hijoIzq.tipo == 'BOOLEAN' or izq == True or izq == False:
                if self.hijoDer.tipo == 'INT' or isinstance(der, int):
                    self.tipo = 'INT'
                    return int((1 if izq else 0)) + self.obtenerValor('INT',der)
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return int((1 if izq else 0)) + self.obtenerValor('DOUBLE', der)
                elif self.hijoDer.tipo == 'BOOLEAN' or isinstance(der, bool):
                    self.tipo = 'INT'
                    return int((1 if izq else 0)) + self.obtenerValor('BOOLEAN', der)
                elif self.hijoDer.tipo == 'STRING' or isinstance(der,str):
                    self.tipo = 'STRING'
                    return str(("true" if izq else "false"))+str(self.obtenerValor('STRING', der))

            elif self.hijoIzq.tipo == 'CHAR' or isinstance(izq, str):
                if self.hijoDer.tipo == 'STRING' or isinstance(der, str):
                    self.tipo = "STRING"
                    return str(self.obtenerValor('CHAR', izq)) + str(self.obtenerValor('CHAR', der))
                elif self.hijoDer.tipo == 'STRING' or isinstance(der, str):
                    self.tipo = "STRING"
                    return str(self.obtenerValor('CHAR', izq)) + str(self.obtenerValor('STRING', der))

        elif self.operador == '-':
            if (self.hijoIzq.tipo == 'INT' or isinstance(izq,int)) and (izq is not True and izq is not False):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'INT'
                    return int(self.obtenerValor('INT', izq)) - int(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(izq,float):
                    self.tipo = "DOUBLE"
                    return float(self.obtenerValor('INT', izq)) - float(self.obtenerValor('DOUBLE', der))
            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(izq,float):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) - int(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = "DOUBLE"
                    return float(self.obtenerValor('DOUBLE', izq)) - float(self.obtenerValor('DOUBLE', der))
                elif self.hijoDer.tipo == 'BOOLEAN' or der == True or der == False:
                    self.tipo = "DOUBLE"
                    return self.obtenerValor('DOUBLE', izq) - float((1 if der else 0))
            elif self.hijoIzq.tipo == 'BOOLEAN' or izq == True or izq == False:
                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    return self.obtenerValor('DOUBLE', izq) - self.obtenerValor('INT', der)
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = "DOUBLE"
                    return float((1 if izq else 0)) - float(self.obtenerValor('DOUBLE', der))


        elif self.operador == "*":
            if (self.hijoIzq.tipo == 'INT' or isinstance(izq,int))  and (izq is not True and izq is not False):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'INT'
                    return int(self.obtenerValor('INT', izq)) * int(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) * float(self.obtenerValor('DOUBLE', der))
            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(izq,float):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) * float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) * float(self.obtenerValor('DOUBLE', der))

        elif self.operador == "/":
            if (self.hijoIzq.tipo == 'INT' or isinstance(izq,int))  and (izq is not True and izq is not False):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) / float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der, float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) / float(self.obtenerValor('DOUBLE', der))

            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(der, float):
                if self.hijoDer.tipo == 'INT' or isinstance(der, int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) / float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der, float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) / float(self.obtenerValor('DOUBLE', der))

        elif self.operador == "**":
            if (self.hijoIzq.tipo == 'INT' or isinstance(izq,int))  and (izq is not True and izq is not False):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'INT'
                    return int(self.obtenerValor('INT', izq)) ** int(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) ** float(self.obtenerValor('DOUBLE', der))

            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(izq,float):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) ** float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) ** float(self.obtenerValor('DOUBLE', der))

        elif self.operador == "%":
            if (self.hijoIzq.tipo == 'INT' or isinstance(izq, int)) and (izq is not True and izq is not False):
                if self.hijoDer.tipo== 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) % float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('INT', izq)) % float(self.obtenerValor('DOUBLE', der))
            elif self.hijoIzq.tipo == 'DOUBLE' or isinstance(izq,float):
                if self.hijoDer.tipo == 'INT' or isinstance(der,int):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) % float(self.obtenerValor('INT', der))
                elif self.hijoDer.tipo == 'DOUBLE' or isinstance(der,float):
                    self.tipo = 'DOUBLE'
                    return float(self.obtenerValor('DOUBLE', izq)) % float(self.obtenerValor('DOUBLE', der))

    def obtenerValor(self, tipo, valor):
        if tipo == 'INT':
            return int(valor)
        elif tipo == 'STRING':
            return str(valor)
        elif tipo == 'DOUBLE':
            return float(valor)
        elif tipo == 'BOOLEAN':
            return bool(valor)
        elif tipo == 'CHAR':
            return str(valor)
