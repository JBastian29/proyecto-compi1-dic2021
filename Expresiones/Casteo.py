from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class Casteo(Instruccion):
    def __init__(self, fila, columna, tipo, expr):
        self.columna = columna
        self.fila = fila
        self.tipo = tipo
        self.expr = expr

    def ejecutar(self, ent):
        expresion: Simbolo = self.expr.ejecutar(ent)
        sepudo = False
        if expresion is not None:
            if self.tipo.upper() == 'INT':
                if self.expr.tipo.upper() == 'DOUBLE':
                    return Simbolo('INT', '', int(expresion), 0).valor
                if self.expr.tipo.upper()  == 'CHAR':
                    return Simbolo('INT', '', ord(expresion), 0).valor
                if self.expr.tipo.upper()  == 'STRING':
                    if str(expresion).isnumeric():
                        return Simbolo('INT', '', int(expresion), 0).valor
                    else:
                        print('ERROR La cadena no es de tipo entero')
                else:
                    print('Error de casteo de: ' + str(self.expr.valor) + " a INT")



            elif self.tipo.upper() == 'DOUBLE':
                if self.expr.tipo.upper() == 'STRING':
                    try:
                        float(expresion)
                        sepudo = True
                        return Simbolo('DOUBLE', '', float(expresion), 0).valor

                    except:
                        sepudo = False
                        print('ERROR La cadena no es de tipo DOUBLE')
                        return Simbolo('ERROR', 'La cadena no es de tipo double', None, 0)
                if self.expr.tipo.upper() == 'INT':
                    return Simbolo('DOUBLE', '', float(expresion), 0).valor
                if self.expr.tipo.upper() == 'CHAR':
                    return Simbolo('DOUBLE', '', ord(expresion), 0).valor
                else:
                    print('Error de casteo de: ' + str(self.expr.valor) + " a DOUBLE")



            elif self.tipo.upper() == 'CHAR':
                if self.expr.tipo.upper() == 'INT':
                    return Simbolo('CHAR', '', chr(expresion), 0).valor
                if self.expr.tipo.upper() == 'DOUBLE':
                    return Simbolo('CHAR', '', chr(int(expresion)), 0).valor
                else:
                    print('Error de casteo de: ' + str(self.expr.valor) + " a CHAR")

            elif self.tipo.upper() == 'STRING':
                if self.expr.tipo.upper() == 'INT':
                    return Simbolo('STRING', '', str(expresion), 0).valor
                if self.expr.tipo.upper() == 'DOUBLE':
                    return Simbolo('STRING', '', str(expresion), 0).valor
                if self.expr.tipo.upper() == 'BOOLEAN':
                    if expresion == True or expresion == "true":
                        return Simbolo('STRING', '', "True", 0).valor
                    elif expresion == False or expresion == "false":
                        return Simbolo('STRING', '', "False", 0).valor
                else:
                    print('Error de casteo de: ' + str(self.expr.valor) + " a STRING")

            elif self.tipo.upper() == 'BOOLEAN':
                if self.expr.tipo.upper() == 'STRING':
                    if expresion == True or expresion.lower() == "true":
                        return Simbolo('STRING', '', "True", 0).valor
                    elif expresion == False or expresion.lower() == "false":
                        return Simbolo('STRING', '', "False", 0).valor
                    else:
                        print('ERROR La cadena no es de tipo BOOLEAN')
                else:
                    print('ERROR La cadena no es de tipo BOOLEAN')
