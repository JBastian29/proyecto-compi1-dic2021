from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo


class Identificador(Instruccion):
    def __init__(self, fila, columna, identificador):
        self.identificador = identificador
        self.fila = fila
        self.columna = columna
        self.tipo = None

    def ejecutar(self, entorno: Entorno):
        simbolo: Simbolo = entorno.buscarSimbolo(self.identificador.lower())

        if simbolo != None:
            self.tipo = simbolo.tipo
            return simbolo.valor

        return None
