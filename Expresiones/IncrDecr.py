from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo


class IncrDecr(Instruccion):
    def __init__(self, fila, columna, identificador, masmenos):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.masmenos = masmenos

    def ejecutar(self, ent: Entorno):
        varActualizar: Simbolo = ent.buscarSimbolo(self.identificador)

        if varActualizar is not None:
            if varActualizar.tipo.upper() == 'INT' or varActualizar.tipo.upper() == 'DOUBLE':
                if self.masmenos == '++':
                    if varActualizar.valor is None:
                        varActualizar.valor = 1
                    else:
                        varActualizar.valor = varActualizar.valor + 1
                elif self.masmenos == '--':
                    if varActualizar.valor is None:
                        varActualizar.valor = -1
                    else:
                        varActualizar.valor = varActualizar.valor - 1

                ret = ent.editarSimbolo(self.identificador, varActualizar)
                return varActualizar
