from Interface.Instruccion import Instruccion


class Primitivo(Instruccion):
    def __init__(self, fila, columna, tipo, valor):
        self.tipo = tipo
        self.valor = valor
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entorno):
        return self.valor
