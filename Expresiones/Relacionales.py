from Interface.Instruccion import Instruccion


class Relacionales(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = None

    def ejecutar(self, entorno):
        izq = self.hijoIzq.ejecutar(entorno)
        der = self.hijoDer.ejecutar(entorno)

        if self.operador == '<':
            return self.obtenerValor2(izq) < self.obtenerValor2(der)
        if self.operador == '>':
            return self.obtenerValor2(izq) > self.obtenerValor2(der)
        if self.operador == '<=':
            return self.obtenerValor2(izq) <= self.obtenerValor2(der)
        if self.operador == '>=':
            return self.obtenerValor2(izq) >= self.obtenerValor2(der)
        if self.operador == "==":
            return self.obtenerValor2(izq) == self.obtenerValor2(der)
        if self.operador == "!=":
            return self.obtenerValor2(izq) != self.obtenerValor2(der)

    def obtenerValor2(self, valor):
        if isinstance(valor,int):
            return int(valor)
        elif isinstance(valor,str):
            return str(valor)
        elif isinstance(valor,float):
            return float(valor)
        elif isinstance(valor,bool):
            return bool(valor)
