import copy
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo
from Entorno.Entorno import Entorno


class Arreglo1(Instruccion):
    def __init__(self, fila, columna, dimensiones, tipo, identificador, cant_dim, otro_tipo):
        self.fila = fila
        self.columna = columna
        self.dimensiones = dimensiones
        self.tipo = tipo
        self.identificador = identificador
        self.cant_dim = cant_dim
        self.otro_tipo = otro_tipo

    def ejecutar(self, ent: Entorno):
        if self.tipo == self.otro_tipo:
            if self.dimensiones == len(self.cant_dim):
                arreglo = self.crearDimensiones(ent, copy.copy(self.cant_dim))
                nuevo_simbolo = Simbolo(self.tipo, str(
                    self.identificador), arreglo, 0)
                crear_simbolo = ent.nuevoSimbolo(nuevo_simbolo)
                if crear_simbolo is not None:
                    return nuevo_simbolo

        return None

    def crearDimensiones(self, ent, cant):
        arreglo = []
        if len(cant) == 0:
            if self.otro_tipo == 'STRING':
                return ""
            elif self.otro_tipo == 'INT':
                return 0
            elif self.otro_tipo == 'DOUBLE':
                return 0.0

        if len(cant) != 0:
            nueva_dimension = cant.pop(0)
            numero=nueva_dimension.ejecutar(ent)

            if numero is not None:
                i = 0
                while i < int(numero):
                    arreglo.append(self.crearDimensiones(ent, copy.copy(cant)))
                    i += 1
                return arreglo
        else:
            return arreglo
