from Interface.Instruccion import Instruccion

class Break(Instruccion):
    def __init__(self, fila, columna):
        self.columna = columna
        self.fila = fila

    def ejecutar(self, entorno):
        return self
