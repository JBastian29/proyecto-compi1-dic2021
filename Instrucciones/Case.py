from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break


class Case(Instruccion):
    def __init__(self, fila, columna, instrucciones, expr):
        self.columna = columna
        self.fila = fila
        self.instrucciones = instrucciones
        self.expr = expr

    def ejecutar(self, entorno):
        entLocal = Entorno(entorno)
        for ins in self.instrucciones:
            r = ins.ejecutar(entLocal)
            if isinstance(r, Break):
                return r
