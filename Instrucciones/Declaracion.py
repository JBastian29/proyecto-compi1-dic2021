from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Interface.Instruccion import Instruccion
from Instrucciones.LlamadaFuncion import LlamadaFuncion
from Instrucciones.GetArreglo import GetArreglo


class Declaracion(Instruccion):
    def __init__(self, fila, columna, identificador, tipo, expresion=None):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.expresion = expresion
        self.tipo = tipo

    def ejecutar(self, entorno: Entorno):

        if self.expresion == None:
            simbolo = Simbolo(self.tipo, str(
                self.identificador).lower(), None, self.fila)
        else:
            valor = self.expresion.ejecutar(entorno)
            # verificar que valor no sea un error o nulo (None)
            simbolo = Simbolo(self.tipo, str(
                self.identificador).lower(), valor, self.fila)

        if self.tipo is not None:
            if self.expresion is None:
                declaracion = entorno.nuevoSimbolo(simbolo)
            elif isinstance(self.expresion,LlamadaFuncion):
                if Entorno.buscarSimbolo(entorno,self.expresion.identificador).valor.tipo.upper() == self.tipo.upper():
                    declaracion = entorno.nuevoSimbolo(simbolo)
                else:
                    print("No se pudo declarar la variable proveniente de una funcion, tipo de dato incompatible")
            elif self.tipo.upper() == self.expresion.tipo.upper():
                declaracion = entorno.nuevoSimbolo(simbolo)
            else:
                print("No se pudo declarar la variable " + str(self.identificador) + " tipo de dato incompatible")

