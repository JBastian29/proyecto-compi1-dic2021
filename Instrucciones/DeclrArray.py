from Entorno.Entorno import Entorno
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo

class DeclrArray(Instruccion):
    def __init__(self,fila,columna,identificador,posicion,expr):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.posicion = posicion
        self.expr = expr

    def ejecutar(self, entorno:Entorno):
        posicion=[]
        for p in self.posicion:
            posicion.append(p.ejecutar(entorno))
        simbl : Simbolo = entorno.buscarSimbolo(str(self.identificador))
        if simbl is not None:
            if len(posicion) == 1:
                x = self.expr.ejecutar(entorno)
                entorno.buscarSimbolo(self.identificador).valor[posicion[0]] = x
            elif len(posicion) == 2:
                x = self.expr.ejecutar(entorno)
                entorno.buscarSimbolo(self.identificador).valor[posicion[0]][posicion[1]] = x
            elif len(posicion) == 3:
                x = self.expr.ejecutar(entorno)
                entorno.buscarSimbolo(self.identificador).valor[posicion[0]][posicion[1]][posicion[2]] = x
