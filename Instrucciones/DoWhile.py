from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno


class DoWhile(Instruccion):
    def __init__(self, fila, columna, condicion, instrucciones):
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entorno: Entorno):
        cond = True
        while(cond):
            for ins in self.instrucciones:
                ins.ejecutar(entorno)
            cond = self.condicion.ejecutar(entorno)
