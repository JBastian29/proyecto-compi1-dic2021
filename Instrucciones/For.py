from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno


class For(Instruccion):
    def __init__(self, fila, columna, asignacion, declaracion, condicion, incrdecr, instrucciones):
        self.columna = columna
        self.fila = fila
        self.asignacion = asignacion
        self.declaracion = declaracion
        self.condicion = condicion
        self.incrdecr = incrdecr
        self.instrucciones = instrucciones

    def ejecutar(self, ent: Entorno):
        entCondicion = Entorno(ent)

        if self.asignacion is not None:
            self.asignacion.ejecutar(entCondicion)
        elif self.declaracion is not None:
            self.declaracion.ejecutar(entCondicion)

        cond = self.condicion.ejecutar(entCondicion)

        if isinstance(cond,bool):
            while cond == True:
                entLocal = Entorno(entCondicion)
                for ins in self.instrucciones:
                    ins.ejecutar(entLocal)

                self.incrdecr.ejecutar(entCondicion)

                cond = self.condicion.ejecutar(entCondicion)
        else:
            print("Tipo de condición no admitida para ciclo FOR")
