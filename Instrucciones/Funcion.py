from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Entorno.Funcion import SimboloFuncion
from Instrucciones.Return import Return


class Funcion(Instruccion):
    def __init__(self, fila, columna, tipo, nombre, parametros, instrucciones):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo
        self.nombre = nombre
        self.parametros = parametros
        self.instrucciones = instrucciones


    def ejecutar(self, ent: Entorno):
        hayReturn = False
        for i in self.instrucciones:
            if isinstance(i, Return):
                hayReturn = True

        if self.tipo.upper() == 'VOID' or hayReturn == True:
            nuevaFuncion = SimboloFuncion(self.instrucciones, self.parametros, self.nombre, self.tipo)
            resultado = ent.nuevoSimbolo(simbolo=Simbolo('FUNC', self.nombre, nuevaFuncion))
            if resultado == None:
                # agregar el error en la lista de errores.
                return None
        else:
            print("La funcion debe traer una instruccion RETURN")
            return None
