from Entorno.Entorno import Entorno
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo

class GetArreglo(Instruccion):
    def __init__(self,fila,columna,identificador,posicion):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.posicion = posicion
        self.tipo = None

    def ejecutar(self, entorno:Entorno):
        posicion = []
        for p in self.posicion:
            posicion.append(p.ejecutar(entorno))
        simbl = entorno.buscarSimbolo(self.identificador)
        self.tipo=simbl.tipo
        if simbl is not None:
            if len(posicion) == 1:
                return entorno.buscarSimbolo(self.identificador).valor[posicion[0]]
            elif len(posicion) == 2:
                return entorno.buscarSimbolo(self.identificador).valor[posicion[0]][posicion[1]]
            elif len(posicion) == 3:
                return entorno.buscarSimbolo(self.identificador).valor[posicion[0]][posicion[1]][posicion[2]]

