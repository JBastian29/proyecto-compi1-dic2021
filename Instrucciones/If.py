from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Return import Return

class If(Instruccion):
    def __init__(self, fila, columna, condicion, instrucciones, lelseif, else_instr):
        self.fila = fila
        self.columna = columna
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.lelseif = lelseif
        self.else_instr = else_instr

    def ejecutar(self, ent: Entorno):
        cicloCumplido = False
        cond = self.condicion.ejecutar(ent)

        # validar que cond no sea None o que sea de tipo booleano
        if isinstance(cond,bool):
            if bool(cond):
                entIf = Entorno(ent)
                for ins in self.instrucciones:
                    ret = ins.ejecutar(entIf)
                    if isinstance(ins,Return):
                        return ins
                    # validar que "ret" no devuelva None o error para reportarlo
            else:
                if self.lelseif is not None:
                    for lelif in self.lelseif:
                        condElseIf = lelif.condicion.ejecutar(ent)
                        # validar que la condElseIf no sea None o que sea de tipo booleano
                        if isinstance(condElseIf,bool):
                            if condElseIf:
                                cicloCumplido = True
                                entElseIf = Entorno(ent)
                                for elsif in lelif.instrucciones:
                                    ret = elsif.ejecutar(entElseIf)
                                    # validar que "ret" no devuelva None o error

                if self.else_instr is not None and not cicloCumplido:
                    entElse = Entorno(ent)
                    for e in self.else_instr:
                        ret = e.ejecutar(entElse)
                        # validar que "ret" no devuelva None o error