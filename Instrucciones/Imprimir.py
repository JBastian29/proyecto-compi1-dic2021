from Interface.Instruccion import Instruccion


class Imprimir(Instruccion):
    def __init__(self, fila, columna, expresion):
        self.fila = fila
        self.columna = columna
        self.expresion = expresion

    def ejecutar(self, entorno):
        valor = self.expresion.ejecutar(entorno)
        print(valor)
