from Interface.Instruccion import Instruccion
from Entorno.Funcion import SimboloFuncion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Nativas.AMinus import AMinus
from Nativas.AMayus import AMayus
from Nativas.Truncate import Truncate
from Nativas.Nround import Nround
from Instrucciones.Return import Return


class LlamadaFuncion(Instruccion):
    def __init__(self, fila, columna, identificador, parametros):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, ent: Entorno):
        func: Simbolo = ent.buscarSimbolo(self.identificador)
        parametrosRecibidos = []

        if func is not None:
            if func.tipo == 'FUNC':
                funcion: SimboloFuncion = func.valor
                self.tipo = funcion.tipo
                if self.parametros is not None:
                    # primero verificar que la cantidad de parametros en la llamada
                    # coincida con la cantidad de parametros que la funcion tiene declarada
                    if len(self.parametros) == len(funcion.param):
                        cont = 0
                        for p in self.parametros:
                            param: Simbolo = p.ejecutar(ent)
                            # luego verificar que cada parametro coincida
                            # en su tipo con los parametros declarados en la funcion
                            if p.tipo.upper() == funcion.param[cont].tipo.upper():
                                parametrosRecibidos.append(
                                    Simbolo(p.tipo, funcion.param[cont].nombre, param))
                            else:
                                print("Tipo de parametro no coincide")
                                return None
                            cont += 1
                    else:
                        print("La cantidad de parametros no coincide")

                # creamos un entorno para la ejecucion de la funcion
                entFuncion = Entorno(ent)

                # agregamos los parametros recibidos en la llamada
                # de la funcion al entorno local
                for p in parametrosRecibidos:
                    s = entFuncion.nuevoSimbolo(p)
                    if s is None:
                        print("No se pudo agregar parametro")
                        return None

                for ins in funcion.instrucciones:
                    r = ins.ejecutar(entFuncion)
                    if isinstance(ins, Return):
                        return r
                    if isinstance(r,Return):
                        return r.ejecutar(entFuncion)

        else:
            iden = str(self.identificador).lower()
            if iden == 'aminus':
                func: AMinus = AMinus(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == 'amayus':
                func: AMayus = AMayus(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == 'truncate':
                func: Truncate = Truncate(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == 'round':
                func: Nround = Nround(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
