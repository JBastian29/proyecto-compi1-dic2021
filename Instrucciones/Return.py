from Interface.Instruccion import Instruccion


class Return(Instruccion):
    def __init__(self, fila, columna, exp):
        self.fila = fila
        self.columna = columna
        self.exp = exp

    def ejecutar(self, entorno):
        e = self.exp.ejecutar(entorno)
        if e is not None:
            return e
