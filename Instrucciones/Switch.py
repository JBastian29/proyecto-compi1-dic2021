from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Instrucciones.Break import Break
from Instrucciones.Return import Return


class Switch(Instruccion):
    def __init__(self, fila, columna, lcases, default, expr):
        self.fila = fila
        self.columna = columna
        self.lcases = lcases
        self.default = default
        self.expr = expr

    def ejecutar(self, ent: Entorno):
        expresion: Simbolo = self.expr.ejecutar(ent)
        yaseejecuto = False
        if expresion is not None:
            if self.lcases is not None:
                for case in self.lcases:
                    evaluacion: Simbolo = case.expr.ejecutar(ent)
                    expresion: Simbolo = self.expr.ejecutar(ent)

                    if evaluacion is not None:
                        yaseejecuto = evaluacion == expresion
                        if yaseejecuto:
                            r = case.ejecutar(ent)
                            if isinstance(r, Break):
                                return
                            if isinstance(r, Return):
                                return r
                            yaseejecuto = False

            if self.default is not None:
                if not yaseejecuto:
                    r = self.default.ejecutar(ent)
                    if isinstance(r, Break):
                        return
                    if isinstance(r, Return):
                        return r
        return None
