from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno


class While(Instruccion):
    def __init__(self, fila, columna, condicion, instrucciones):
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entorno):
        cond = self.condicion.ejecutar(entorno)
        while(cond):
            for a in self.instrucciones:
                a.ejecutar(entorno)
            cond=self.condicion.ejecutar(entorno)
