from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class AMinus(Instruccion):
    def __init__(self, fila, columna, parametros):
        self.columna = columna
        self.fila = fila
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, entorno):
        listParam = []
        listParam.append(self.parametros)
        if self.parametros is not None:
            if len(listParam) == 1:
                param = listParam[0].ejecutar(entorno)
                if listParam[0].tipo.upper() == 'STRING':
                    #print("DESDE NATIVA -- " + str(param).lower())
                    self.tipo = 'STRING'
                    return Simbolo('STRING', '', str(param).lower(), 0).valor
                else:
                    print("El parametro para la funcion aMinus debe ser de tipo STRING")
            else:
                print("La funcion aMinus debe traer solamente un parametro")
        else:
            print("La funcion aMinus debe traer un parametro")
        return None
