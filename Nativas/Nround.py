import math
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class Nround(Instruccion):
    def __init__(self, fila, columna, parametros):
        self.columna = columna
        self.fila = fila
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, entorno):
        listParam = []
        listParam.append(self.parametros)
        if self.parametros is not None:
            if len(listParam) == 1:
                param = listParam[0].ejecutar(entorno)
                if listParam[0].tipo.upper() == 'DOUBLE':
                    #print("DESDE NATIVA -- " + str(param).lower())
                    if param - int(param) >= 0.5:
                        self.tipo = 'INT'
                        return Simbolo('INT', '', int(param) + 1, 0).valor
                    else:
                        self.tipo = 'INT'
                        return Simbolo('INT', '', int(param), 0).valor
                else:
                    print("El parametro para la funcion truncate debe ser de tipo INT")
            else:
                print("La funcion truncate debe traer solamente un parametro")
        else:
            print("La funcion truncate debe traer un parametro")
        return None