import math
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class Tamano(Instruccion):
    def __init__(self, fila, columna, parametros):
        self.columna = columna
        self.fila = fila
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, entorno):
        listParam = []
        listParam.append(self.parametros)
        if self.parametros is not None:
            if len(listParam) == 1:
                param = listParam[0].ejecutar(entorno)
                if isinstance(param,list):
                    #print("DESDE NATIVA -- " + str(param).lower())
                    self.tipo = 'INT'
                    return Simbolo('INT', '', len(param), 0).valor
                else:
                    print("El parametro para la funcion TAMAÑO debe ser de tipo lista")
            else:
                print("La funcion TAMAÑO debe traer solamente un parametro")
        else:
            print("La funcion tamaño debe traer un parametro")
        return None