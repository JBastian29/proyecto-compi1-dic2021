import math
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class Truncate(Instruccion):
    def __init__(self, fila, columna, parametros):
        self.columna = columna
        self.fila = fila
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, entorno):
        listParam = []
        listParam.append(self.parametros)
        if self.parametros is not None:
            if len(listParam) == 1:
                param = listParam[0].ejecutar(entorno)
                if listParam[0].tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    #print("DESDE NATIVA -- " + str(param).lower())
                    return Simbolo('INT', '', int(math.trunc(param)), 0).valor
                else:
                    print("El parametro para la funcion truncate debe ser de tipo DOUBLE")
            else:
                print("La funcion truncate debe traer solamente un parametro")
        else:
            print("La funcion truncate debe traer un parametro")
        return None