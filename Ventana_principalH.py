import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QRect, QSize
from PyQt5.QtWidgets import QAction,QApplication,QFileDialog, QMainWindow,QWidget
from PyQt5.QtGui import QColor, QPainter, QTextFormat, QKeySequence, QFont
from gramatica import cargarDatos
from ventana_principal import Ui_MainWindow

class Editor_Texto(QMainWindow):
    def __init__(self):
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.envr = None
        self.ui.Btn_generar.clicked.connect(self.ejecutar)
        self.ui.actionAbrir.triggered.connect(self.abrir)
        self.show()

    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__



    @QtCore.pyqtSlot(str)
    def on_myStream_message(self, message):
        self.ui.plainTextEdit_3.insertPlainText(message)
