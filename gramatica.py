import re
import ply.yacc as yacc
import ply.lex as lex
from Expresiones.Aritmetica import Aritmetica
from Expresiones.Primitivo import Primitivo
from Expresiones.Identificador import Identificador
from Instrucciones.Imprimir import Imprimir
from Instrucciones.Declaracion import Declaracion
from Entorno.Entorno import Entorno
from Instrucciones.Asignacion import Asignacion
from Interface.Error import Error
from Expresiones.Relacionales import Relacionales
from Instrucciones.While import While
from Instrucciones.If import If
from Expresiones.IncrDecr import IncrDecr
from Instrucciones.For import For
from Instrucciones.DoWhile import DoWhile
from Entorno.TablaSimbolos import TablaSimbolos
from Instrucciones.Break import Break
from Instrucciones.Return import Return
from Instrucciones.Case import Case
from Instrucciones.Switch import Switch
from Expresiones.Casteo import Casteo
from Nativas.AMinus import AMinus
from Nativas.AMayus import AMayus
from Nativas.Truncate import Truncate
from Nativas.Nround import Nround
from Instrucciones.Funcion import Funcion
from Instrucciones.LlamadaFuncion import LlamadaFuncion
from Instrucciones.Parametros import Parametro
from Instrucciones.Arreglo1 import Arreglo1
from Instrucciones.DeclrArray import DeclrArray
from Instrucciones.GetArreglo import GetArreglo
from Nativas.Tamaño import Tamano


reservadas = {
    'while': 'while',
    'cond': 'cond',
    'instr': 'instr',
    'int': 'r_int',
    'string': 'r_string',
    'boolean': 'r_boolean',
    'imprimir': 'imprimir',
    'double': 'r_double',
    'char': 'r_char',
    'if': 'if',
    'else': 'else',
    'for': 'for',
    'break': 'break',
    'return': 'return',
    'switch': 'switch',
    'case': 'case',
    'default': 'default',
    'expr': 'expr',
    'do': 'do',
    'true': 'true',
    'false': 'false',
    'aminus': 'aminus',
    'amayus': 'amayus',
    'truncate': 'truncate',
    'round': 'round',
    'tamano': 'tamano',
    'expr': 'expr',
    'new': 'new',
    'func': 'func',
    'tipo': 'tipo',
    'nombre': 'nombre',
    'void': 'void',
    'param': 'param',
    'tablasimbolos': 'tablasimbolos'
}

t_dosp = r':'
t_pyc = r';'
t_llavea = r'{'
t_llavec = r'}'
t_para = r'\('
t_parc = r'\)'
t_corchetea = r'\['
t_corchetec = r'\]'
t_coma = r','
t_mas = r'\+'
t_menos = r'-'
t_or = r'\|\|'
t_and = r'&&'
t_not = r'!'
t_igualigual = r'=='
t_diferente = r'!='
t_menorque = r'<'
t_menorigual = r'<='
t_mayorque = r'>'
t_mayorigual = r'>='
t_dividido = r'/'
t_por = r'\*'
t_modulo = r'%'
t_potencia = r'\*\*'
t_masmas = r'\+\+'
t_menosmenos = r'--'

tokens = [
    'dosp',
    'pyc',
    'llavea',
    'llavec',
    'para',
    'parc',
    'corchetea',
    'corchetec',
    'coma',
    'mas',
    'menos',
    'or',
    'and',
    'not',
    'igualigual',
    'diferente',
    'menorque',
    'menorigual',
    'mayorque',
    'mayorigual',
    'dividido',
    'por',
    'modulo',
    'potencia',
    'masmas',
    'menosmenos',
    'double',
    'int',
    'cadenaString',
    'char',
    'id'
] + list(reservadas.values())

def t_boolean(t):
    r'^([t][r][u][e]|[f][a][l][s][e])$'
    try:
        t.value = bool(t.value)
    except ValueError:
        print("Valor bool incorrecto %d", t.value)
        t.value = 0
    return t

def t_double(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Valor numerico double incorrecto %d",t.value)
        t.value=0
    return t

def t_int(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Valor numerico int incorrecto %d", t.value)
        t.value = 0
    return t

def t_char(t):
    r'"[a-zA-Z]"|\'[a-zA-Z0-9\!\@\#\$\%\^\&\*\)\(\+\=\.\_\-]\''

    try:
        t.value = str(t.value)
        t.value = t.value[1:-1]
    except ValueError:
        print("Valor char incorrecto %d", t.value)
        t.value = ""
    return t


def t_cadenaString(t):
    r'"([^\"\n\r\\]|\\n|\\r|\\t|\\"|\\\'|\\\\)*?"'
    t.value = t.value[1:-1]  # remuevo las comillas
    t.value = t.value.replace('\\n', '\n')
    t.value = t.value.replace('\\N', '\n')
    t.value = t.value.replace('\\r', '\r')
    t.value = t.value.replace('\\R', '\r')
    t.value = t.value.replace('\\t', '\t')
    t.value = t.value.replace('\\T', '\t')
    t.value = t.value.replace('\\\'', '\'')
    t.value = t.value.replace('\\"', '\"')
    t.value = t.value.replace('\\\\', '\\')
    return t



def t_id(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reservadas.get(t.value.lower(), 'id')
    return t

t_ignore = " \t\r"

# Compute column.
#     input is the input text string
#     token is a token instance


def find_column(inp, token):
    '''line_start = inp.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1'''
    return 1


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    listaErrores.append(
        Error("Léxico", "Caracter invalido '%s'" % t.value[0], 0, 0))
    t.lexer.skip(1)

def t_COMENTARIO_MULTILINEA(t):
    r'\#&([^&\#]|[^&]/|&[^\#])*&\#'
    t.lexer.lineno = t.value.count('\n')

def t_COMENTARIO_SIMPLE(t):
    r'\#.*\n'
    t.lexer.lineno+=1

lexer = lex.lex(reflags = re.IGNORECASE)

# precedencia de operadores

precedence = (
    ('left', 'or'),
    ('left', 'and'),
    ('right', 'not'),
    ('left', 'igualigual', 'diferente', 'menorque',
     'menorigual', 'mayorque', 'mayorigual'),
    ('left', 'mas', 'menos'),
    ('left', 'dividido', 'por', 'modulo'),
    ('nonassoc', 'potencia'),
    ('left', 'masmas', 'menosmenos'),
    ('right', 'negativo'),
)


# definición de la gramática


def p_init(t):
    'INIT : LINSTRUCCIONES'
    t[0] = t[1]


def p_linstrucciones(t):
    'LINSTRUCCIONES : LINSTRUCCIONES INSTRUCCIONES'
    t[1].append(t[2])
    t[0] = t[1]


def p_linstrucciones2(t):
    'LINSTRUCCIONES : INSTRUCCIONES'
    t[0] = [t[1]]


def p_instrucciones(t):
    '''INSTRUCCIONES : WHILE
                    | DECL
                    | IMPRIMIR
                    | ASIGNACION
                    | INCRDECR pyc
                    | IF_INSTR
                    | FOR_INSTR
                    | BREAK_INSTR
                    | RETURN_INSTR
                    | SWITCH_INSTR
                    | DO_WHILE
                    | DECL_ARR
                    | FUNCIONES
                    | LLAMADA_FUNC
                    | ASIGNACION_ARR
                    | TABLA_SIMBOLOS'''
    t[0] = t[1]

def p_llamadafunc(t):
    'LLAMADA_FUNC : id para LVALORES parc'
    t[0] = LlamadaFuncion(0, 0, t[1], t[3])


def p_llamadafunc2(t):
    'LLAMADA_FUNC : id para parc'
    t[0] = LlamadaFuncion(0, 0, t[1], None)


def p_funciones(t):
    'FUNCIONES : func dosp llavea tipo dosp TIPO coma nombre dosp id coma param dosp corchetea LPARAM corchetec coma INSTR  llavec'
    t[0] = Funcion(0, 0, t[6], t[10], t[15], t[18])


def p_funciones2(t):
    'FUNCIONES : func dosp llavea tipo dosp TIPO coma nombre dosp id coma param dosp corchetea corchetec coma INSTR  llavec'
    t[0] = Funcion(0, 0, t[6], t[10], None, t[17])


def p_lparam1(t):
    'LPARAM : LPARAM coma TIPO id'
    t[1].append(Parametro(t[3], t[4]))
    t[0] = t[1]


def p_lparam2(t):
    'LPARAM : TIPO id'
    t[0] = [Parametro(t[1], str(t[2]))]


def p_decl_arr(t):
    '''
    DECL_ARR : ARR_TIPO1
            | ARR_TIPO2
    '''
    t[0] = t[1]


'string [] arr1 : new string[7+1]'

def p_arr1(t):
    'ARR_TIPO1 : TIPO LDIMENSIONES id dosp new TIPO LDIMEXP pyc'
    t[0] = Arreglo1(0, 0, t[2], t[1], t[3], t[7], t[6])


def p_ldimensiones(t):
    'LDIMENSIONES : LDIMENSIONES corchetea corchetec'
    t[0] = t[1] + 1


def p_ldimensiones2(t):
    'LDIMENSIONES : corchetea corchetec'
    t[0] = 1


def p_ldimexp1(t):
    'LDIMEXP : LDIMEXP corchetea E corchetec'
    t[1].append(t[3])
    t[0] = t[1]


def p_ldimexp2(t):
    'LDIMEXP : corchetea E corchetec'
    t[0] = [t[2]]


def p_arr2(t):
    'ARR_TIPO2 : TIPO LDIMENSIONES id dosp LARREGLOS pyc'


def p_larreglos1(t):
    'LARREGLOS : llavea LVALORES llavec'
    t[0] = t[2]


def p_lvalores(t):
    'LVALORES : LVALORES coma E'
    t[1].append(t[3])
    t[0] = t[1]


def p_lvalores2(t):
    'LVALORES : E'
    t[0] = [t[1]]


def p_larreglo2(t):
    'LARREGLOS : llavea LRECURSIVO llavec'
    t[0] = t[2]


def p_lrecursivo1(t):
    'LRECURSIVO : LRECURSIVO coma LVALORES'
    t[1].append(t[3])
    t[0] = t[1]


def p_lrecursivo2(t):
    'LRECURSIVO : LVALORES'
    t[0] = [t[1]]

def p_declArray(t):
    'ASIGNACION_ARR : id LDIMEXP dosp E pyc'
    t[0] = DeclrArray(t.lineno(1),t.lexpos(1),t[1],t[2],t[4])

def p_switch1(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma LCASES coma DEFAULT_INS llavec'
    t[0] = Switch(0, 0, t[8], t[10], t[6])


def p_switch2(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma LCASES llavec'
    t[0] = Switch(0, 0, t[8], None, t[6])


def p_switch3(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma DEFAULT_INS llavec'
    t[0] = Switch(0, 0, None, t[8], t[6])


def p_lcases1(t):
    'LCASES : LCASES coma CASES'
    t[1].append(t[3])
    t[0] = t[1]


def p_lcases2(t):
    'LCASES : CASES'
    t[0] = [t[1]]


def p_cases(t):
    'CASES : case dosp llavea expr dosp E coma INSTR llavec'
    t[0] = Case(0, 0, t[8], t[6])


def p_default(t):
    'DEFAULT_INS : default dosp llavea INSTR llavec'
    t[0] = Case(0, 0, t[4], None)

def p_break(t):
    'BREAK_INSTR : break pyc'
    t[0] = Break(0, 0)

def p_return(t):
    'RETURN_INSTR : return E pyc'
    t[0] = Return(0, 0, t[2])


def p_forinstr(t):
    'FOR_INSTR : for dosp llavea cond dosp para ASIGNACION E pyc INCRDECR parc coma INSTR llavec'
    t[0] = For(0, 0, t[7], None, t[8], t[10], t[13])


def p_forinstr2(t):
    'FOR_INSTR : for dosp llavea cond dosp para DECL E pyc INCRDECR parc coma INSTR llavec'
    t[0] = For(0, 0, None, t[7], t[8], t[10], t[13])

def p_if_instr1(t):
    '''IF_INSTR : if dosp llavea COND coma INSTR llavec'''
    t[0] = If(0, 0, t[4], t[6], None, None)


def p_if_instr2(t):
    '''IF_INSTR : if dosp llavea COND coma INSTR llavec else dosp llavea INSTR llavec'''
    t[0] = If(0, 0, t[4], t[6], None, t[11])


def p_if_instr3(t):
    '''IF_INSTR : if dosp llavea COND coma INSTR llavec LELSEIF'''
    t[0] = If(0, 0, t[4], t[6], t[8], None)


def p_if_instr(t):
    '''IF_INSTR : if dosp llavea COND coma INSTR llavec LELSEIF else dosp llavea INSTR llavec'''
    t[0] = If(0, 0, t[4], t[6], t[8], t[12])


def p_lelseif(t):
    '''LELSEIF : LELSEIF ELSEIF'''
    t[1].append(t[2])
    t[0] = t[1]


def p_lelseif2(t):
    'LELSEIF : ELSEIF'
    t[0] = [t[1]]


def p_elseif(t):
    '''ELSEIF : else if dosp llavea COND coma INSTR llavec'''
    t[0] = If(0, 0, t[5], t[7], None, None)


def p_asignacion(t):
    'ASIGNACION : id dosp E pyc'
    t[0] = Asignacion(t.lineno(1), find_column(
        input, t.slice[1]), str(t[1]), t[3])


def p_asignacionError(t):
    'ASIGNACION : id dosp error pyc'
    listaErrores.append(Error(
        "Sintáctico", "Se esperaba una expresión en la asignación de variables", 0, 0))


'''
var2 : 54;
var1 : { ;

# instrucciones
'''


def p_imprimir(t):
    'IMPRIMIR : imprimir para E parc pyc'
    t[0] = Imprimir(t.lineno(1), find_column(
        input, t.slice[1]), t[3])

def p_stable(t):
    'TABLA_SIMBOLOS : tablasimbolos para E parc pyc'
    t[0] = TablaSimbolos(t[3])

def p_decl(t):
    'DECL : TIPO id dosp E pyc'
    t[0] = Declaracion(t.lineno(1), find_column(
        input, t.slice[1]), str(t[2]), t[1], t[4])


def p_decl2(t):
    'DECL : TIPO id pyc'
    t[0] = Declaracion(t.lineno(1), find_column(
        input, t.slice[1]), str(t[2]), t[1])


def p_while(t):
    'WHILE : while dosp llavea COND coma INSTR llavec'
    t[0] = While(t.lineno(1), find_column(input, t.slice[1]), t[4], t[6])


def p_do_while(t):
    'DO_WHILE : do dosp llavea INSTR coma while dosp llavea COND llavec llavec'
    t[0]  = DoWhile(t.lineno(1), find_column(input, t.slice[1]),t[9],t[4])


def p_cond(t):
    'COND : cond dosp para E parc'
    t[0] = t[4]

def p_instr(t):
    'INSTR : instr dosp llavea LINSTRUCCIONES llavec'
    t[0] = t[4]


def p_tipo(t):
    '''TIPO : r_string
    | r_int
    | r_double
    | r_boolean
    | r_char
    | void'''
    t[0] = str(t[1]).lower()

def p_eFuncion(t):
    'E : LLAMADA_FUNC'
    t[0] = t[1]

def p_eArreglosUno(t):
    'E : id LDIMEXP'
    t[0] = GetArreglo(t.lineno(1),t.lexpos(1),t[1],t[2])

def p_incrDecr(t):
    '''INCRDECR : id masmas
        | id menosmenos
    '''
    t[0] = IncrDecr(0, 0, str(t[1]), str(t[2]))

def p_eOperacionesPar(t):
    'E : para E parc'
    t[0]=t[2]

def p_eAritmetica(t):
    '''E : E mas E 
        | E menos E
        | E por E
        | E dividido E
        | E potencia E
        | E modulo E'''
    t[0] = Aritmetica(t.lineno(1), find_column(
        input, t.slice[1]), str(t[2]), t[1], t[3])


def p_eLogicas(t):
    '''E : E and E
        | E or E
    '''


def p_eRelacional(t):
    '''E : E menorque E
        | E mayorque E
        | E menorigual E
        | E mayorigual E
        | E igualigual E
        | E diferente E 
    '''
    t[0] = Relacionales(t.lineno(1), find_column(input, t.slice[1]), t[2], t[1], t[3])


def p_eUnarias(t):
    '''E : not E
        | menos E %prec negativo
    '''

def p_eIncDec(t):
    'E : INCRDECR '
    t[0] = t[1]

def p_eAminus(t):
    'E : aminus para E parc'
    t[0] = AMinus(0, 0, t[3])

def p_eAmayus(t):
    'E : amayus para E parc'
    t[0] = AMayus(0, 0, t[3])

def p_eTruncate(t):
    'E : truncate para E parc'
    t[0] = Truncate(0, 0, t[3])

def p_eRound(t):
    'E : round para E parc'
    t[0] = Nround(0, 0, t[3])

def p_eTamano(t):
    'E : tamano para E parc'
    t[0] = Tamano(0, 0, t[3])

def p_eCasteo(t):
    'E : corchetea TIPO corchetec E'
    t[0] = Casteo(0, 0, t[2], t[4])

def p_edouble(t):
    'E : double'
    t[0] = Primitivo(t.lineno(1), find_column(input, t.slice[1]), 'DOUBLE', float(t[1]))

def p_e(t):
    'E : int'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), 'INT', int(t[1]))

def p_echar(t):
    'E : char'
    t[0] = Primitivo(t.lineno(1), find_column(input, t.slice[1]), 'CHAR', str(t[1]))

def p_e2(t):
    'E : cadenaString'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), 'STRING', str(t[1]))

def p_eboolean(t):
    'E : true'
    t[0] = Primitivo(t.lineno(1), find_column(input, t.slice[1]), 'BOOLEAN', True)

def p_eboolean2(t):
    'E : false'
    t[0] = Primitivo(t.lineno(1), find_column(input, t.slice[1]), 'BOOLEAN', False)


def p_eId(t):
    'E : id'
    t[0] = Identificador(t.lineno(1), find_column(
        input, t.slice[1]), str(t[1]))


def p_error(t):
    print(t)
    print("Error sintáctico en '%s'" % t.value)



parser = yacc.yacc()
listaErrores = []


def analizar():
    global listaErrores

def cargarDatos(entrada):
    instrucciones = parser.parse(entrada)
    entornoGlobal = Entorno(None)
    try:
        for ins in instrucciones:
            ins.ejecutar(entornoGlobal)
    except:
        print("--Ocurrio un problema al momento de ejecutar la instruccion--")
    return entornoGlobal


ej = '''

    imprimir();
    imprimir(null);

    int y : 0;
    int x : 7;

    | y = 0
    | x = 7

    y : x++;

    | x = 8
    | y = 8


    class If(Instruccion):
        def __init:
            self.fila
            self.condicion # a la vez es es una expresion que tambien implementa Instruccion
            self.instrucciones # listado de clases que implementan Instruccion
            self.instrElse # listado de clases que implementan Instruccion y se encuentran en ELSE
        
        def ejecutar(self, ent:Entorno):
            cond = self.condicion.ejecutar(ent)

            if != None:
                if cond.tipo == 'BOOL':
                    if cond.valor:
                        nuevoEntorno = Entorno(ent)
                        for ins in self.instrucciones:
                            ins.ejecutar(nuevoEntorno)
                    else:
                        if self.instrElse is not None:
                            entElse = Entorno(ent)
                            for ins in self.instElse:
                                ins.ejecutar(entElse)

'''





