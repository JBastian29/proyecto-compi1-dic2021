from graphviz import Digraph
import ply.yacc as yacc
import ply.lex as lex

reservadas = {
    'while': 'while',
    'cond': 'cond',
    'instr': 'instr',
    'int': 'r_int',
    'string': 'r_string',
}

t_dosp = r':'
t_pyc = r';'
t_llavea = r'{'
t_llavec = r'}'
t_para = r'\('
t_parc = r'\)'
t_coma = r','

tokens = [
    'dosp',
    'pyc',
    'llavea',
    'llavec',
    'para',
    'parc',
    'coma',
    'int',
    'cadenaString',
    'id'
] + list(reservadas.values())


def t_int(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Valor numerico incorrecto %d", t.value)
        t.value = 0
    return t


def t_cadenaString(t):
    r'".*?"'
    t.value = t.value[1:-1]  # remuevo las comillas
    return t


def t_id(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reservadas.get(t.value.lower(), 'id')
    return t


t_ignore = " \t\r"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    t.lexer.skip(1)


lexer = lex.lex()

arbol = Digraph(comment='Árbol generado en clase 4, compi 1',
                node_attr={'fillcolor': 'darkolivegreen', 'color': 'darkolivegreen',
                           'style': 'filled', 'fontcolor': 'white'},
                edge_attr={'arrowsize': '0.7'}, graph_attr={'label': 'Árbol Sintáctico Concreto (CST)'})

i = 0


def idNodo():
    global i
    i += 1
    return str(i)

# precedencia de operadores

# definición de la gramática


def p_init(t):
    'INIT : LINSTRUCCIONES'
    id = idNodo()
    t[0] = id
    arbol.node(id, "INIT")
    arbol.edge(id, t[1])


def p_linstrucciones(t):
    'LINSTRUCCIONES : LINSTRUCCIONES INSTRUCCIONES'
    t[0] = t[1]
    arbol.edge(t[1], t[2])


def p_linstrucciones2(t):
    'LINSTRUCCIONES : INSTRUCCIONES'
    id = idNodo()
    t[0] = id
    arbol.node(id, "INSTRUCCION")
    arbol.edge(id, t[1])


def p_instrucciones(t):
    '''INSTRUCCIONES : WHILE
                    | DECL '''
    t[0] = t[1]


def p_decl(t):
    'DECL : TIPO id dosp E pyc'
    id = idNodo()
    t[0] = id
    arbol.node(id, "DECL")
    arbol.edge(id, t[1])
    idId = idNodo()
    arbol.node(idId, str(t[2]))
    idDosP = idNodo()
    arbol.node(idDosP, str(t[3]))
    idPyc = idNodo()
    arbol.node(idPyc, str(t[5]))
    arbol.edge(id, t[4])
    arbol.edge(id, idId)
    arbol.edge(id, idDosP)
    arbol.edge(id, idPyc)


def p_while(t):
    'WHILE : while dosp llavea COND coma INSTR llavec'
    id = idNodo()
    t[0] = id
    arbol.node(id, "WHILE")
    idWhile = idNodo()
    arbol.node(idWhile, str(t[1]))
    idDosP = idNodo()
    arbol.node(idDosP, str(t[2]))
    idLlavea = idNodo()
    arbol.node(idLlavea, str(t[3]))
    idComa = idNodo()
    arbol.node(idComa, str(t[5]))
    idLlavec = idNodo()
    arbol.node(idLlavec, str(t[7]))
    arbol.edge(id, t[4])
    arbol.edge(id, t[6])
    arbol.edge(id, idWhile)
    arbol.edge(id, idDosP)
    arbol.edge(id, idLlavea)
    arbol.edge(id, idLlavec)
    arbol.edge(id, idComa)


def p_cond(t):
    'COND : cond dosp para E parc'
    id = idNodo()
    t[0] = id
    arbol.node(id, "COND")
    idCond = idNodo()
    arbol.node(idCond, str(t[1]))
    idDosP = idNodo()
    arbol.node(idDosP, str(t[2]))
    idPara = idNodo()
    arbol.node(idPara, str(t[3]))
    idParc = idNodo()
    arbol.node(idParc, str(t[5]))
    arbol.edge(id, t[4])
    arbol.edge(id, idDosP)
    arbol.edge(id, idPara)
    arbol.edge(id, idParc)
    arbol.edge(id, idCond)


def p_instr(t):
    'INSTR : instr dosp llavea LINSTRUCCIONES llavec'
    id = idNodo()
    t[0] = id
    arbol.node(id, "INSTR")
    idInstr = idNodo()
    arbol.node(idInstr, str(t[1]))
    idDosP = idNodo()
    arbol.node(idDosP, str(t[2]))
    idLlavea = idNodo()
    arbol.node(idLlavea, str(t[3]))
    idLlavec = idNodo()
    arbol.node(idLlavec, str(t[5]))
    arbol.edge(id, t[4])
    arbol.edge(id, idDosP)
    arbol.edge(id, idInstr)
    arbol.edge(id, idLlavea)
    arbol.edge(id, idLlavec)


def p_tipo(t):
    '''TIPO : r_string 
    | r_int'''

    id = idNodo()
    t[0] = id
    idTipo = idNodo()
    arbol.node(id, "TIPO")
    arbol.node(idTipo, str("Tipo de dato: " + t[1]))
    arbol.edge(id, idTipo)


def p_e(t):
    '''E : int 
    | cadenaString'''

    id = idNodo()
    t[0] = id
    arbol.node(id, "E")
    idE = idNodo()
    arbol.node(idE, str(t[1]))
    arbol.edge(id, idE)


def p_error(t):
    print(t)
    print("Error sintáctico en '%s'" % t.value)


parser = yacc.yacc()

entrada = '''
int puntos : 0;
while : {
    cond : (100),
    instr : {
        string cadenita : "si sale compi1 en vacas :D";
    }
}
'''

parser.parse(entrada)

arbol.render('arbolito', view=True)
'arbolito.pdf'
