import sys

from PyQt5 import QtCore
from ventana_principal import Ui_MainWindow
from PyQt5.QtWidgets import QApplication, QMainWindow

class HandlerQ(QMainWindow):
    def __init__(self):
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__

    @QtCore.pyqtSlot(str)
    def on_myStream_message(self, message):
        self.ui.plainTextEdit_3.insertPlainText(message)

class MyStream(QtCore.QObject):
    message = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(MyStream, self).__init__(parent)

    def write(self, message):
        self.message.emit(str(message))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    principal = HandlerQ()
    principal.show()
    myStream = MyStream()
    myStream.message.connect(principal.on_myStream_message)
    sys.stdout = myStream
    sys.exit(app.exec_())


